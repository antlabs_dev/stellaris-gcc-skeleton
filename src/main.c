#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/timer.h"
#include "utils/uartstdio.h"

#define LED_RED GPIO_PIN_1
#define LED_BLUE GPIO_PIN_2
#define LED_GREEN GPIO_PIN_3

// Timer0 interrupt service routine
void Timer0IntHandler(void)
{
	// Clear interrupt flag so prevent re-enter ISR on exit
	TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

	static volatile unsigned long counter = 0;  
	
	IntMasterDisable();
	UARTprintf("Timer interrupt %u\n", ++counter);
	IntMasterEnable();
}

int main(void) 
{
	// Enable lazy stacking for interrupt handlers.  This allows floating-point
 	// instructions to be used within interrupt handlers, but at the expense of
    	// extra stack usage.
	FPULazyStackingEnable();
	// Setup CPU clock
	SysCtlClockSet(SYSCTL_SYSDIV_2_5 | SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ | SYSCTL_OSC_MAIN);
  	// Enable PORTF GPIO
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	// Set PORTF pins to output
  	GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, LED_RED | LED_BLUE | LED_GREEN);
  	// Enable PORTA GPIO
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
	// Configure PORTA pins for UART0
	GPIOPinConfigure(GPIO_PA0_U0RX);
    	GPIOPinConfigure(GPIO_PA1_U0TX);
	// Enable UART0
    	GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
	// Init UARTprintf library
	UARTStdioInit(0);
	// Enable Timer0 clock and power 
	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
    	// Configure the 32-bit periodic timer.
    	TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);
    	TimerLoadSet(TIMER0_BASE, TIMER_A, SysCtlClockGet());
    	// Setup the interrupts for the timer timeout
    	IntEnable(INT_TIMER0A);
    	TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
    	// Enable the timer
    	TimerEnable(TIMER0_BASE, TIMER_A);
	// Enable global interrupts
	IntMasterEnable();
	
	UARTprintf("CPU core clock is %u MHz\n", SysCtlClockGet() / 1000ul / 1000ul);
	
	while(1) 
 	{
		static volatile unsigned long counter = 0;  
		
		// Blink LED
		GPIOPinWrite(GPIO_PORTF_BASE, LED_RED | LED_GREEN | LED_BLUE, LED_GREEN);
		SysCtlDelay(SysCtlClockGet() / 3ul);
		GPIOPinWrite(GPIO_PORTF_BASE, LED_RED | LED_GREEN | LED_BLUE, 0); 
		SysCtlDelay(SysCtlClockGet() / 3ul);
	
		IntMasterDisable();	
		UARTprintf("Cycle iteration %u\n", ++counter);
		IntMasterEnable();
	}
}
